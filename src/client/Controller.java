package client;

import common.entities.Car;
import common.serviceInterfaces.ISellingService;
import common.serviceInterfaces.ITaxService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Controller {
    private ITaxService taxService;
    private ISellingService sellingService;

    @FXML
    private TextField yearText;
    @FXML
    private TextField engineText;
    @FXML
    private TextField priceText;
    @FXML
    private TextField resultText;

    @FXML
    private Button computeTaxButton;
    @FXML
    private Button sellingPriceButton;

    public Controller(){
        try {
            Registry registry = LocateRegistry.getRegistry(8889);

            taxService = (ITaxService) registry.lookup("ITaxService");
            sellingService = (ISellingService) registry.lookup("ISellingService");
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }

    @FXML
    private void computeTaxHandler(ActionEvent e){
        try{
            int year = Integer.parseInt(yearText.getText());
            int engineSize = Integer.parseInt(engineText.getText());
            double price = Double.parseDouble(priceText.getText());
            Car c = new Car(year, engineSize, price);
            Double result = taxService.computeTax(c);
            resultText.setText(String.valueOf(result));
        }
        catch(Exception ex){
            resultText.setText("ERROR!!! The fields should be completed with numbers!");
        }
    }

    @FXML
    private void sellingPriceHandler(ActionEvent e) {
        try{
            int year = Integer.parseInt(yearText.getText());
            int engineSize = Integer.parseInt(engineText.getText());
            double price = Double.parseDouble(priceText.getText());
            Car c = new Car(year, engineSize, price);
            Double result = sellingService.computeSellingPrice(c);
            resultText.setText(String.valueOf(result));
        }
        catch(Exception ex){
            resultText.setText("ERROR!!! The fields should be completed with numbers!");
        }
    }
}
