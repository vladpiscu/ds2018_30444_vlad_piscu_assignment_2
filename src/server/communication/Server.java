package server.communication;

import common.serviceInterfaces.ISellingService;
import common.serviceInterfaces.ITaxService;
import server.services.SellingService;
import server.services.TaxService;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {
    private static final int PORT = 8889;

    public Server(){
        try {
            SellingService sellingService = new SellingService();
            TaxService taxService = new TaxService();

            ISellingService sellingServiceStub = (ISellingService) UnicastRemoteObject.exportObject(sellingService, PORT);
            ITaxService taxServiceStub = (ITaxService) UnicastRemoteObject.exportObject(taxService, PORT);

            Registry registry = LocateRegistry.createRegistry(PORT);

            System.setProperty("java.rmi.server.hostname", String.valueOf(PORT));
            registry.bind("ISellingService", sellingServiceStub);
            registry.bind("ITaxService", taxServiceStub);
            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

}
