package common.serviceInterfaces;

import common.entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ISellingService extends Remote {
    double computeSellingPrice(Car car) throws RemoteException;
}
