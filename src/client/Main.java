package client;

import common.entities.Car;
import common.serviceInterfaces.ISellingService;
import common.serviceInterfaces.ITaxService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader viewLoader = new FXMLLoader(getClass().getResource("view.fxml"));
        viewLoader.setController(new Controller());
        Parent root = viewLoader.load();
        primaryStage.setTitle("Car service");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
