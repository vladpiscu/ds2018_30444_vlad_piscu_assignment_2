package server.services;

import common.entities.Car;
import common.serviceInterfaces.ISellingService;

public class SellingService implements ISellingService {
    @Override
    public double computeSellingPrice(Car car) {
        if(2018 - car.getYear() >= 7)
            return 0;
        return car.getPrice() - car.getPrice() / 7 * (2018 - car.getYear());
    }
}
