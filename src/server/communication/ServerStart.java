package server.communication;

import java.io.IOException;

public class ServerStart {

    private ServerStart() {
    }

    public static void main(String[] args) {
        new Server();
        System.out.println("The server started.");
    }
}
